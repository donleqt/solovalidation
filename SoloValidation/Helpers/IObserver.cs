﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoloValidation
{
    public interface IObserver
    {
        void Update(NotifySubject subject);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SoloValidation
{
    class TextBlockErrorBindingHelper : ErrorBindingHelper
    {
        public TextBlockErrorBindingHelper(TextBlock txtBlock, string propertyName )
        : base(txtBlock, propertyName)
        {

        }

        protected override void SetTargetValue(string errorMessage)
        {
            dynamic txtBlock = this._target;
            txtBlock.Text = errorMessage;
        }
    }
}

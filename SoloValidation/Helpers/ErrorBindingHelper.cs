﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoloValidation
{
    public abstract class ErrorBindingHelper : IObserver
    {
        protected object _target;
        protected string _propertyName;
        public void Update(NotifySubject subject)
        {
            ValidatableBase instance = (ValidatableBase)subject;
            string errorMessage = "";
            if (instance.ValidationErrors.ContainsKey(this._propertyName)) {
                errorMessage = instance.ValidationErrors[this._propertyName];
            }
            this.SetTargetValue(errorMessage);
        }

        protected ErrorBindingHelper(object target, string propertyName)
        {
            this._target = target;
            this._propertyName = propertyName;
        }

        abstract protected void SetTargetValue(string errorMessage);

    }
}

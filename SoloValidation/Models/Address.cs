﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoloValidation.Models
{
    public class Address : ValidatableBase
    {
        [RequiredValidator()]
        public string street { get; set; }
        [RequiredValidator()]
        public string ward { get; set; }
        [RequiredValidator()]
        public string district { get; set; }
        [RequiredValidator()]
        public string city { get; set; }
    }
}

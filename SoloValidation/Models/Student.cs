﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoloValidation.Models
{
    public class Student : ValidatableBase
    {
        [RequiredValidator(),LengthValidator(5, 255)]
        public string name { get; set; }
        [RegexValidator("^\\d+$"), RequiredValidator()]
        public string id { get; set; }

        [RequiredValidator(), RangeValidator(1, 10)]
        public int gpa { get; set; }

        [SoloValidation.Atrributes.NestedValidator()]
        public Address address { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SoloValidation.Models;
namespace SoloValidation
{
   

    public partial class MainWindow : Window
    {
        Student modelStudent;

        public MainWindow()
        {
            InitializeComponent();
            this.modelStudent = new Student();
            modelStudent.address = new Address();

            this.NewStudentForm.DataContext = modelStudent;
            PairErrorMessage();
            
        }
        private void SaveStudentInfo(object sender, RoutedEventArgs e)
        {
            if (modelStudent.IsValid)
                MessageBox.Show("This student infomation is valid", "Successful");
        }

        private void PairErrorMessage()
        {
            List<TextBlockErrorBindingHelper> list = new List<TextBlockErrorBindingHelper>();
            list.Add(new TextBlockErrorBindingHelper(txtNameErrMsg, "name"));
            list.Add(new TextBlockErrorBindingHelper(txtIdErrMsg, "id"));
            list.Add(new TextBlockErrorBindingHelper(txtGpaErrMsg, "gpa"));
            list.Add(new TextBlockErrorBindingHelper(txtStreetErrMsg, "address"));

            foreach (var e in list)
            {   // Attach vào modelStudent 
                this.modelStudent.Attach(e);
            }
        }
}
    
}
﻿using System;
using System.Collections.Generic;
using System.Reflection;

using SoloValidation.Validators;

namespace SoloValidation.Atrributes
{
    class NestedValidatorAttribute: ValidatorAttribute
    {
        public override Validator GetValidator(PropertyInfo propertyInfo)
        {
            return new NestedValidator(this.ErrorMessage, propertyInfo);
        }
    }
}

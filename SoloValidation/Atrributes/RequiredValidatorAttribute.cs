using System;
using System.Collections.Generic;
using System.Reflection;

using SoloValidation.Validators;

namespace SoloValidation
{
    public class RequiredValidatorAttribute : ValidatorAttribute
    {
        public override Validator GetValidator(PropertyInfo propertyInfo)
        {
            return new RequiredValidator(this.ErrorMessage, propertyInfo);
        }

    }
}
using System;
using System.Collections.Generic;
using System.Reflection;

using SoloValidation.Validators;

namespace SoloValidation
{
    public class CompareValidatorAttribute : ValidatorAttribute
    {

        private ComparisonOperator comparisonOperator;
        private object valueToCompare;


        public ComparisonOperator ComparisonOperator
        {
            get { return this.comparisonOperator; }
        }

  
        public object ValueToCompare
        {
            get { return this.valueToCompare; }
        }

        public CompareValidatorAttribute(ComparisonOperator comparisonOperator, object valueToCompare)
        {
            this.comparisonOperator = comparisonOperator;
            this.valueToCompare = valueToCompare;
        }

        public override Validator GetValidator(PropertyInfo propertyInfo)
        {
            return new CompareValidator(this.ErrorMessage, propertyInfo, this.comparisonOperator, this.valueToCompare);
        }

    }
}
using System;
using System.Collections.Generic;
using System.Reflection;

using SoloValidation.Validators;

namespace SoloValidation
{
    public class LengthValidatorAttribute : ValidatorAttribute
    {

        private uint maxLength;
        private uint minLength;

        public uint MaxLength
        {
            get { return this.maxLength; }
        }


        public uint MinLength
        {
            get { return this.minLength; }
        }

        public LengthValidatorAttribute(uint maxLength)
            : this(uint.MinValue, maxLength)
        {
        }

        public LengthValidatorAttribute(uint minLength, uint maxLength)
        {
            this.maxLength = maxLength;
            this.minLength = minLength;
        }

        public override Validator GetValidator(PropertyInfo propertyInfo)
        {
            return new LengthValidator(this.ErrorMessage, propertyInfo, this.minLength, this.maxLength);
        }
    
    }
}
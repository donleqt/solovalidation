using System;
using System.Collections.Generic;
using System.Reflection;

using SoloValidation.Validators;

namespace SoloValidation
{

    public class RegexValidatorAttribute : ValidatorAttribute
    {

        private string regex;

        public string Regex
        {
            get { return this.regex; }
        }

        public RegexValidatorAttribute(string regex)
        {
            this.regex = regex;
        }

        public override Validator GetValidator(PropertyInfo propertyInfo)
        {
            return new RegexValidator(this.ErrorMessage, propertyInfo, this.regex);
        }

    }
}
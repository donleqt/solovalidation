using System;
using System.Collections.Generic;
using System.Reflection;

using SoloValidation.Validators;

namespace SoloValidation
{
    public abstract class ValidatorAttribute : Attribute
    {

        private string errorMessage;
        private uint order;

        public string ErrorMessage
        {
            get { return this.errorMessage; }
            set { this.errorMessage = value; }
        }

        public uint Order
        {
            get { return this.order; }
            set { this.order = value; }
        }
        public abstract Validator GetValidator(PropertyInfo propertyInfo);

        public ValidatorAttribute()
        {
            this.errorMessage = null;
            this.order = 0;
        }

    }
}

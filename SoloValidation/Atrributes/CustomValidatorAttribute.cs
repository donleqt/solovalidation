using System;
using System.Collections.Generic;
using System.Reflection;

using SoloValidation.Validators;

namespace SoloValidation
{
  
    public class CustomValidatorAttribute : ValidatorAttribute
    {

        private ICustomValidationRule customBridge;

        public ICustomValidationRule CustomBridge
        {
            get { return this.customBridge; }
            set { this.customBridge = value; }
        }

        public CustomValidatorAttribute(string errorMessage, ICustomValidationRule customBridge)
        {
            this.ErrorMessage = errorMessage;
            this.customBridge = customBridge;
        }

        public override Validator GetValidator(PropertyInfo propertyInfo)
        {
            return new CustomValidator(this.ErrorMessage, propertyInfo, customBridge);
        }
    }

}
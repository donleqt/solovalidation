using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SoloValidation
{
    //Validatable objects.
    public class ValidatableBase : NotifySubject 
    {

        
        private ValidationManager validationManager;


        public bool IsValid
        {
            get
            {
                bool result =  this.validationManager.IsValid();
                // Notify for all listeners
                this.Notify();
                return result;
            }
        }

        public Dictionary<string, string> ValidationErrors
        {
            get { return this.validationManager.ValidationErrors; }
        }

       
        protected ValidatableBase()
        {
            this.validationManager = new ValidationManager(this);
        }

        
    }
}

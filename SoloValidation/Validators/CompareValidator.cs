using System;
using System.Collections.Generic;
using System.Reflection;

namespace SoloValidation.Validators
{
  
    public class CompareValidator : Validator
    {

        private ComparisonOperator comparisonOperator;
        private object valueToCompare;

        public ComparisonOperator ComparisonOperator
        {
            get { return this.comparisonOperator; }
        }

        public object ValueToCompare
        {
            get { return this.valueToCompare; }
        }

        protected override Type[] ValidPropertyTypes
        {
            get { return new Type[] { typeof(IComparable) }; }
        }

        public CompareValidator(PropertyInfo propertyInfo, ComparisonOperator comparisonOperator, object valueToCompare)
            : this(null, propertyInfo, comparisonOperator, valueToCompare)
        {
        }

        public CompareValidator(string errorMessage, PropertyInfo propertyInfo, ComparisonOperator comparisonOperator, object valueToCompare)
            : base(errorMessage, propertyInfo)
        {
            this.comparisonOperator = comparisonOperator;
            this.valueToCompare = valueToCompare;
            if (string.IsNullOrEmpty(errorMessage))
                this.ErrorMessage = string.Format(Resources.DefaultErrorMessages.Compare, propertyInfo.Name, this.comparisonOperator.ToString(), this.valueToCompare);
        }

        protected override bool DoIsValid(object instance, object value)
        {
            IComparable comp = value as IComparable;

            object valueToCompare = Convert.ChangeType(this.valueToCompare, value.GetType());

            return TestComparisonResult(comp.CompareTo(valueToCompare));
        }

        private bool TestComparisonResult(int result)
        {
            switch (this.comparisonOperator)
            {
                case ComparisonOperator.Equal:
                    return result == 0;
                case ComparisonOperator.NotEqual:
                    return result != 0;
                case ComparisonOperator.GreaterThan:
                    return result > 0;
                case ComparisonOperator.GreaterThanOrEqual:
                    return result >= 0;
                case ComparisonOperator.LessThan:
                    return result < 0;
                case ComparisonOperator.LessThanOrEqual:
                    return result <= 0;
            }
            return false;
        }

        public static CompareValidator CreateValidator<T>(string propertyName, ComparisonOperator comparisonOperator, object valueToCompare)
        {
            return CreateValidator(typeof(T), propertyName, comparisonOperator, valueToCompare);
        }
        public static CompareValidator CreateValidator(Type type, string propertyName, ComparisonOperator comparisonOperator, object valueToCompare)
        {
            return new CompareValidator(Validator.GetPropertyInfo(type, propertyName), comparisonOperator, valueToCompare);
        }

        public static CompareValidator CreateValidator<T>(string errorMessage, string propertyName, ComparisonOperator comparisonOperator, object valueToCompare)
        {
            return CreateValidator(typeof(T), errorMessage, propertyName, comparisonOperator, valueToCompare);
        }

        public static CompareValidator CreateValidator(Type type, string errorMessage, string propertyName, ComparisonOperator comparisonOperator, object valueToCompare)
        {
            return new CompareValidator(errorMessage, Validator.GetPropertyInfo(type, propertyName), comparisonOperator, valueToCompare);
        }
    }
}

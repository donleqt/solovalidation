using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace SoloValidation.Validators
{
    public class CustomValidator : Validator
    {

        ICustomValidationRule customBridge;
        protected override Type[] ValidPropertyTypes { get { return null;  } }

        public CustomValidator(string errorMessage, PropertyInfo propertyInfo, ICustomValidationRule customBrige)
            : base(errorMessage, propertyInfo)
        {
            this.customBridge = customBrige;
        }

        protected override bool DoIsValid(object instance, object value)
        {
            return this.customBridge.DoIsValid(value);
        }

        public static CustomValidator CreateValidator<T>(string errorMessage, string propertyName, ICustomValidationRule customBrige)
        {
            return CreateValidator(typeof(T), errorMessage, propertyName, customBrige);
        }

  
        public static CustomValidator CreateValidator(Type type, string errorMessage, string propertyName, ICustomValidationRule customBrige)
        {
            return new CustomValidator(errorMessage, Validator.GetPropertyInfo(type, propertyName), customBrige);
        }

    }
}

using System;
using System.Collections.Generic;
using System.Reflection;

namespace SoloValidation.Validators
{
   
    public abstract class Validator
    {

        private string errorMessage;
        private PropertyInfo propertyInfo;

        public string ErrorMessage
        {
            get { return this.errorMessage; }
            protected set { this.errorMessage = value; }
        }
        public PropertyInfo PropertyInfo
        {
            get { return this.propertyInfo; }
        }

        protected abstract Type[] ValidPropertyTypes { get; }

        protected Validator(string errorMessage, PropertyInfo propertyInfo)
        {
            if (string.IsNullOrEmpty(errorMessage))
                this.errorMessage = string.Format(Resources.DefaultErrorMessages.Default, propertyInfo.Name);
            else
                this.errorMessage = errorMessage;

            this.propertyInfo = propertyInfo;

            ThrowIfInvalidPropertyType();
        }



        // Check if fan instace is valid
        public bool IsValid(object instance)
        {
            object value = this.GetValue(instance);
            return DoIsValid(instance, value);
        }

        // Step get value
       protected object GetValue(object instance)
        {
            object value = this.propertyInfo.GetValue(instance, null);
            return value;
        }
        // Do specified validate, child class will implement this
        protected abstract bool DoIsValid(object instance, object value);

       // Check if goodType can compare with test type
        protected bool AreCompatibleTypes(Type goodType, Type testType)
        {
            if (testType == goodType)
                return true;

            Type underTestType = Nullable.GetUnderlyingType(testType);
            if (underTestType != null && underTestType == goodType)
                return true;

            if (goodType.IsInterface)
            {
                foreach (Type iface in testType.GetInterfaces())
                    if (iface == goodType)
                        return true;
            }

            if (testType.IsSubclassOf(goodType))
                return true;

            return false;
        }

        // Check errror if type can't not be validate
        protected virtual void ThrowIfInvalidPropertyType()
        {
            if (this.ValidPropertyTypes == null || this.ValidPropertyTypes.Length == 0)
                return;

            Type propType = this.propertyInfo.PropertyType;
            foreach (Type type in this.ValidPropertyTypes)
            {
                if (AreCompatibleTypes(type, propType))
                    return;
            }

            throw new InvalidValidatorException(this.GetType(), propType);
        }

       
        // Get property info
        protected static PropertyInfo GetPropertyInfo(Type type, string propertyName)
        { 
            return type.GetProperty(propertyName);
        }


    }
}
using System;
using System.Runtime.Serialization;

namespace SoloValidation
{
    [Serializable]
    public class InvalidValidatorException : InvalidOperationException
    {

        private Type propertyType;
        private Type validatorType;


        public Type PropertyType
        {
            get { return this.propertyType; }
        }

 
        public Type ValidatorType
        {
            get { return this.validatorType; }
        }

        public InvalidValidatorException(Type validatorType, Type propertyType)
            : base(string.Format("{0} cannot be applied to a(n) {1}.",validatorType.FullName,propertyType.FullName))
        {
            this.propertyType = propertyType;
            this.validatorType = validatorType;
        }

    }
}
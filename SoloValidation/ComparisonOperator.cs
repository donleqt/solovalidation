using System;
using System.Collections.Generic;
using System.Text;

namespace SoloValidation
{
    public enum ComparisonOperator
    {
 
        Equal,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
        NotEqual
    }
}

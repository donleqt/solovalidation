using System;
using System.Collections.Generic;
using System.Reflection;

using SoloValidation.Validators;
using SoloValidation;

using ValidatorCollection = System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<SoloValidation.Validators.Validator>>;


    public sealed class ValidationManager
    {
        //Prototype list store validators of a type
        private static Dictionary<Type, ValidatorCollection> typeValidators;

        private object target;
        private Type targetType;
        private Dictionary<string, string> validationErrors;
        private ValidatorCollection validators;
        private ValidatorCollection instanceValidators;

        // Public field to get validators list
        public ValidatorCollection Validators
        {
            get { return GetValidators(); }
        }

        // Public field to get errors list
        public Dictionary<string, string> ValidationErrors
        {
                get { return this.validationErrors; }
        }

        static ValidationManager()
        {
            typeValidators = new Dictionary<Type, ValidatorCollection>(); 
        }

        // Attach validation for a instance of a type
        public ValidationManager(object instance)
        { 
            this.target = instance;
            this.targetType = instance.GetType();
            this.validationErrors = new Dictionary<string, string>();

        }
        
        // Add validator
        public void AddValidator(Validator validator)
        {
            string key = validator.PropertyInfo.Name;
            if (this.instanceValidators == null)
                this.instanceValidators = new Dictionary<string, List<Validator>>();
            if (!this.instanceValidators.ContainsKey(key))
                this.instanceValidators[key] = new List<Validator>();
            this.instanceValidators[key].Add(validator);

            this.validators = null;
        }

        // Check valid for one element 
        public bool IsPropertyValid(string propertyName)
        {
            ValidatorCollection validators = this.Validators;
            this.validationErrors.Remove(propertyName);
            DoIsValid(validators[propertyName]);
            return this.validationErrors.ContainsKey(propertyName);
        }

      // Check valid for all instance
        public bool IsValid()
        {
            this.validationErrors.Clear();
            ValidatorCollection validators = this.Validators;
            foreach (List<Validator> vl in validators.Values)
                DoIsValid(vl);

            return this.validationErrors.Count == 0;
        }
        
        // Does validation,
        // Check validation for all properties of object
        private void DoIsValid(List<Validator> validators)
        {
            if (validators == null)
                return;

            for (int i = 0; i < validators.Count; i++)
            {
                if (!validators[i].IsValid(this.target))
                {
                    this.validationErrors[validators[i].PropertyInfo.Name] = validators[i].ErrorMessage;
                    break;
                }
            }
        }

        // Get validators list
        private ValidatorCollection GetValidators()
        {
            if (this.validators == null)
                this.validators = ValidationManager.FindValidatorAttributes(this.targetType);

            if (this.instanceValidators != null)
            {
                foreach (KeyValuePair<string, List<Validator>> kvp in this.instanceValidators)
                    this.validators[kvp.Key].AddRange(kvp.Value);
            }

            return this.validators;
        }
     
        // Add global validator
        public static void AddGlobalValidator<T>(Validator validator)
        {
            AddGlobalValidator(typeof(T),validator);
        }

       // Add global validator
        public static void AddGlobalValidator(Type type, Validator validator)
        {
            if (!typeValidators.ContainsKey(type))
                typeValidators[type] = new ValidatorCollection();

            ValidatorCollection validators = typeValidators[type];

            string key = validator.PropertyInfo.Name;
            if (!validators.ContainsKey(key))
                validators[key] = new List<Validator>();

            validators[key].Add(validator);

        }

        // Check valid for an instance
        public static bool IsValid(object instance)
        {
            ValidationManager vm = new ValidationManager(instance);
            return vm.IsValid();
        }

        // Get validators from attribute
        private static ValidatorCollection FindValidatorAttributes(Type type)
        {
            if (!typeValidators.ContainsKey(type))
            {
                ValidatorCollection validators = new ValidatorCollection();
                PropertyInfo[] pis = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);
                    
                foreach (PropertyInfo pi in pis)
                {
                    ValidatorAttribute[] vas = (ValidatorAttribute[])pi.GetCustomAttributes(typeof(ValidatorAttribute), true);
                    List<ValidatorAttribute> vasList = new List<ValidatorAttribute>(vas);
                    vasList.Sort(
                        delegate(ValidatorAttribute va1, ValidatorAttribute va2)
                        {
                            return va1.Order.CompareTo(va2.Order);
                        });

                    List<Validator> list = vasList.ConvertAll<Validator>(
                        delegate (ValidatorAttribute va)
                        {
                            return va.GetValidator(pi);
                        });

                    validators.Add(pi.Name, list);
                }
                typeValidators.Add(type, validators);
            }

            return typeValidators[type];
        }

        
    
}

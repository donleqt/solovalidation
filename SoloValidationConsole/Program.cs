﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoloValidation;
using System.Reflection;

namespace SoloValidationConsole
{
    class Program
    {
        public class Student:ValidatableBase
        {
            [RequiredValidator()]
            public string name { get; set; }
            [RegexValidator("\\d+$")]
            public string mssv { get; set; }

            public Student(string _name, string _mssv)
            {
                this.name = _name;
                this.mssv = _mssv;
            }

        }

        static void Main(string[] args)
        {
            Student don = new Student("Lê Quý Đôn", "22231w2a");
            Console.WriteLine("Helle every body {0}", don.IsValid);
        }
    }
}
